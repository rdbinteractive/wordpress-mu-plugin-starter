# RDB Interactive MU Plugin Starter.
Requires the RDB Interactive WordPress Development Environment. This package does not contain or load all of its dependencies independently of the framework at this time.

At this time, this package should only be installed as part of the environment scaffold for a new theme.

`@todo: Make the above sentence untrue.`

# Navigation
- Creates Custom Primary Navigation Walker to add accessibility attributes.
- Creates Social Media Navigation Walker.
- Enqueues minimal JavaScript to control adding/removing CSS hooks and attributes to navigation items.
- JavaScript `src` files are compiled by the `Mix` process in the main environment for cache busting.
- Navigation styling is controlled via the theme. This plugin provides functionality only.

# Sidebar

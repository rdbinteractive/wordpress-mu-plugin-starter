<?php

namespace RDBIFunc;

class RDBIBootstrap
{
    // Init functionality of all mu-plugins.
    public function __construct()
    {
        (new Navigation\Init());
    }
}

(new RDBIBootstrap());

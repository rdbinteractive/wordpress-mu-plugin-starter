/* global jQuery */
import { initNav } from "./initNav";
import { openNav, closeNav } from "./navMenu";
import { handleSubMenu } from "./handleSubMenu";

jQuery(function ($) {
    "use strict";

    /**
     * Handle Navigation
     */
    const navTrigger = $(".jsNavTrigger");
    const navContainer = $(".jsPrimaryNavigation");
    const primaryTopLevelLinks = $(".jsPrimaryNavigation>li>a");
    const allTopLevelLinks = $(
        ".jsPrimaryNavigation>.menu-item-has-children>a"
    );
    const allMenuLinks = navContainer.find("a");
    const subMenuWrapper = $(".menu-item-has-children");

    /**
     * Set all tab indexes to 0 for collapsed nav.
     * Add sub menu toggle buttons.
     */
    initNav(primaryTopLevelLinks, allTopLevelLinks, true);

    window.addEventListener("resize", function () {
        setTimeout(function () {
            initNav(primaryTopLevelLinks, allTopLevelLinks, false);
        }, 500);
    });

    const topLevelButtons = $(".jsPrimaryNavigation>li>button");
    const allButtons = navContainer.find("button");
    const subMenuToggle = $(".jsSubMenuTrigger");

    /**
     * Open/Close Primary Navigation.
     */
    navTrigger.on("click", function () {
        if (navContainer.hasClass("jsIsOpen")) {
            closeNav(navTrigger, navContainer, allMenuLinks, allButtons);
        } else {
            openNav(
                navTrigger,
                navContainer,
                primaryTopLevelLinks,
                topLevelButtons
            );
        }
    });

    navTrigger.on("keydown", function (event) {
        const key = event.keyCode;

        // Down arrow. Open sub menu.
        if (key === 40) {
            openNav(
                navTrigger,
                navContainer,
                primaryTopLevelLinks,
                topLevelButtons
            );
        }

        // Up arrow. Close sub menu.
        if (key === 38) {
            closeNav(navTrigger, navContainer, allMenuLinks, allButtons);
        }
    });

    subMenuToggle.on("click", function () {
        const toggle = this;
        if ($(toggle).parent().hasClass("jsIsOpen")) {
            handleSubMenu(toggle, "close");
        } else {
            handleSubMenu(toggle, "open");
        }
    });

    subMenuToggle.on("keydown", function (event) {
        const toggle = this;
        const key = event.keyCode;

        // Down arrow. Open sub menu.
        if (key === 40) {
            handleSubMenu(toggle, "open");
        }

        // Up arrow. Close sub menu.
        if (key === 38) {
            handleSubMenu(toggle, "close");
        }
    });

    subMenuWrapper.mouseover(function () {
        if (window.innerWidth >= 1366) {
            const toggle = $(this).find(".jsSubMenuTrigger").get(0);
            handleSubMenu(toggle, "open");
        }
    });

    subMenuWrapper.mouseout(function () {
        if (window.innerWidth >= 1366) {
            const toggle = $(this).find(".jsSubMenuTrigger").get(0);
            handleSubMenu(toggle, "close");
        }
    });
});

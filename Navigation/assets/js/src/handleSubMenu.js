/* global $ */

export function handleSubMenu(toggle, direction) {
    const container = $(toggle).parent();
    const subMenu = container.find(".jsSubMenu:first");
    const subLinks = subMenu.find("a");
    const subTrigger = container.find(".jsSubMenuTrigger");
    const screenReaderTextContainer = $(toggle).find(".ScreenReaderText");
    const topLevelElement = toggle.parentNode.querySelector("a");
    const screenReaderText = topLevelElement.text;

    if (direction === "open") {
        openSubMenu(toggle);
    }

    if (direction === "close") {
        closeSubMenu(toggle);
    }

    function openSubMenu(toggle) {
        /**
         * Set Aria Attributes
         */
        toggle.setAttribute("aria-expanded", "true");
        topLevelElement.setAttribute("aria-expanded", "true");

        /**
         * Set tab indexes.
         */
        subLinks.each(function (id, link) {
            link.setAttribute("tabindex", "0");
        });

        subTrigger.attr("tabindex", "0");

        /**
         * Set screen reader text.
         */
        screenReaderTextContainer.html(
            'close submenu for "' + screenReaderText + '"'
        );

        /**
         * Add visual display classes.
         */
        container.addClass("jsIsOpen");
        subMenu.addClass("jsIsOpen");
        $(toggle).addClass("jsIsOpen");
    }

    function closeSubMenu(toggle) {
        /**
         * Set Aria attributes.
         */
        toggle.setAttribute("aria-expanded", "false");
        topLevelElement.setAttribute("aria-expanded", "false");

        /**
         * Set tab indexes.
         */
        subLinks.each(function (id, link) {
            link.setAttribute("tabindex", "-1");
        });

        /**
         * Set screen reader text.
         */
        screenReaderTextContainer.html(
            'open submenu for "' + screenReaderText + '"'
        );

        /**
         * Remove visual display classes.
         */
        container.removeClass("jsIsOpen");
        subMenu.removeClass("jsIsOpen");
        $(toggle).removeClass("jsIsOpen");
    }
}

import { handleSubMenu } from "./handleSubMenu";
export { openNav, closeNav };

function openNav(navTrigger, navContainer, topLevelLinks, topLevelButtons) {
    /**
     * Add open class to container and trigger.
     */
    navContainer.addClass("jsIsOpen");
    navTrigger.addClass("jsNavOpen");

    /**
     * Set aria-expanded to true.
     */
    navTrigger.attr("aria-expanded", "true");
    navTrigger.find(".ScreenReaderText").html("Close Primary Navigation");

    /**
     * Add the top level links to the tab index.
     */
    topLevelLinks.each(function (id, link) {
        link.setAttribute("tabindex", "0");
    });

    /**
     * Add the top level buttons to the tab index.
     */
    topLevelButtons.each(function (id, button) {
        button.setAttribute("tabindex", "0");
    });
}

function closeNav(navTrigger, navContainer, allMenuLinks, allButtons) {
    /**
     * Remove open class from container and trigger.
     */
    navContainer.removeClass("jsIsOpen");
    navTrigger.removeClass("jsNavOpen");

    /**
     * Set aria-expanded to false.
     */
    navTrigger.attr("aria-expanded", "false");
    navTrigger.find(".ScreenReaderText").html("Open Primary Navigation");

    /**
     * Remove all links from tab index.
     */
    allMenuLinks.each(function (id, link) {
        link.setAttribute("tabindex", "-1");
    });

    /**
     * Remove all buttons from tab index.
     * Set all buttons aria-expanded to false.
     * Close the sub nav.
     */
    allButtons.each(function (id, button) {
        button.setAttribute("tabindex", "-1");
        button.setAttribute("aria-expanded", "false");
        handleSubMenu(button, "close");
    });
}

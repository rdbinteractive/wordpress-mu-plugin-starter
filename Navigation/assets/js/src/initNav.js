export function initNav(topLevelLinks, allSubMenuLinks, addButtons) {
    /**
     * Set tab index to -1.
     * If JavaScript is available, the menu is collapsed, and shouldn't be in the tab order.
     */
    topLevelLinks.each(function (id, link) {
        if (window.innerWidth < 1366) {
            link.setAttribute("tabindex", "-1");
        }
    });

    /**
     * Add sub menu toggle buttons on first page load.
     */
    if (addButtons === true) {
        allSubMenuLinks.each(function (id, link) {
            // This item's text.
            const itemText = this.text;
            let tabIndex = "";

            if (window.innerWidth < 1366) {
                tabIndex = "-1";
            } else {
                tabIndex = "0";
            }

            // Button to insert.
            const insertedButton =
                '<button class="jsSubMenuTrigger" tabindex="' +
                tabIndex +
                '" aria-expanded="false">' +
                "<span>" +
                '<span class="ScreenReaderText">open submenu for "' +
                "" +
                itemText +
                "" +
                '"</span>' +
                "</span>" +
                "</button>";

            this.insertAdjacentHTML("afterend", insertedButton);
        });
    }
}

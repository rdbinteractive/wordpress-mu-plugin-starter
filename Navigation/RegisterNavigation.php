<?php

namespace RDBIFunc\Navigation;

use RDBIFunc\Navigation\Lib\PrimaryNavWalker;
use RDBIFunc\Navigation\Lib\SocialNavWalker;

class RegisterNavigation
{

    public function register()
    {
        add_action('after_setup_theme', array($this, 'registerNavigation'));
    }

    public function registerNavigation()
    {
        register_nav_menu('primary-nav', __('Primary Navigation', 'rdbi_nav'));
        register_nav_menu('category-nav', __('Category Navigation', 'rdbi_nav'));
        register_nav_menu('social-nav', __('Social Navigation', 'rdbi_nav'));
        register_nav_menu('footer-nav', __('Footer Navigation', 'rdbi_nav'));
    }

    public function rdbiPrimaryNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Primary Navigation', 'rdbi_nav'),
            'menu_class'     => 'PrimaryNavigation jsPrimaryNavigation',
            'theme_location' => 'primary-nav',
            'depth'          => 0,
            'fallback_cb'    => '__return_false',
            'walker' => new PrimaryNavWalker(),
        ));
    }

    public function rdbiCategoryNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Category Navigation', 'rdbi_nav'),
            'menu_class'     => 'CategoryNavigation',
            'theme_location' => 'category-nav',
            'depth'          => 0,
            'fallback_cb'    => '__return_false'
        ));
    }

    public function rdbiSocialNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Social Navigation', 'rdbi_nav'),
            'menu_class'     => 'SocialNavigation',
            'theme_location' => 'social-nav',
            'depth'          => 1,
            'fallback_cb'    => '__return_false',
            'walker'         => new SocialNavWalker(),
        ));
    }

    public function rdbiFooterNav()
    {
        wp_nav_menu(array(
            'container'      => false,
            'menu'           => __('Footer Navigation', 'rdbi_nav'),
            'menu_class'     => 'FooterNavigation',
            'theme_location' => 'footer-nav',
            'depth'          => 0,
            'fallback_cb'    => '__return_false'
        ));
    }
}

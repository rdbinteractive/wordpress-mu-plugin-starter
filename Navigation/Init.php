<?php

namespace RDBIFunc\Navigation;

class Init
{
    public function __construct()
    {
        (new RegisterNavigation())->register();
        (new EnqueueScripts())->enqueue();
    }
}

<?php

namespace RDBIFunc\Navigation;

use RDBIFunc\Lib\Mix;

class EnqueueScripts
{
    /**
     * Enqueue Scripts.
     */
    public function enqueue(): void
    {
        if (!is_admin()) {
            add_action(
                'wp_enqueue_scripts',
                [$this, 'enqueueScripts']
            );
        }
    }

    public function enqueueScripts(): void
    {
        $scriptPath =
            '/app/wp-content/mu-plugins/Navigation/assets/js/primaryNavigation.min.js';

        /** @noinspection PhpUnhandledExceptionInspection */
        wp_enqueue_script(
            'ie-tech-primary-navigation-js',
            (new Mix())->mix($scriptPath),
            'jquery',
            '',
            true
        );
    }
}

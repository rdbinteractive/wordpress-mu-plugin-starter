<?php

namespace RDBIFunc\Lib;

use Exception;

class Mix
{
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string $path
     * @return string
     *
     * @throws Exception
     */
    public function mix(string $path): string
    {
        $manifestURL = MANIFEST_URL;
        $manifest = json_decode(file_get_contents(MANIFEST_DIR . '/mix-manifest.json'), true);
        if (!array_key_exists($path, $manifest)) {
            throw new Exception(
                "Unable to locate Mix file: {$path}. Please check your " .
                'webpack.mix.js output paths and try again.'
            );
        }

        return $manifestURL . str_replace('/app', '', $manifest[$path]);
    }
}
